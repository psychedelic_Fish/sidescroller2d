﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{
    /* Event Set up for when a level piece is removed, will more then likely only be used in GameController
     * but just incase it is set up as an event*/
    public delegate void PieceRemoved(); 
    public static event PieceRemoved pieceRemoved;

    [SerializeField] private float moveSpeed; //How fast objects in the world will move
    private void Start()
    {
        moveSpeed = GameController.instance.WorldMoveSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameController.instance.MoveWorld)
        {
            var temp = (Vector2)transform.position; //Grab a reference to the objects position
            temp += Vector2.left * moveSpeed * Time.deltaTime; //Apply our moveSpeed in the direction of left of screen
            transform.position = temp; //Apply our new position to the objects position

            //Grab the direction of the player to the level piece
            var direction = gameObject.transform.position - GameController.instance.Player.position;
            //Grab the x coorinate of the direction Vector
            var mag = direction.x;

            if (mag < GameController.instance.MaxXDistance) //If the x coorinate less then the set distance
            {
                gameObject.SetActive(false); //Set the gameobject to false (returns the gameObject to the pool)
                pieceRemoved(); //Call the piece removed event
            }
        }
    }

    
}
