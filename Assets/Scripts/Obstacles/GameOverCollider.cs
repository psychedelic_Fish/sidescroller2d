﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverCollider : MonoBehaviour
{
    public delegate void GameOver();
    public static event GameOver gameOver;

    private void OnTriggerEnter(Collider other)
    {
        if (GameController.instance.GameOverHit)
        {
            return;
        }
        if (other.gameObject.CompareTag("Player"))
        {
            GameController.instance.GameOverHit = true;
            gameOver();
        }
        else
        {
            return;
        }
    }
}
