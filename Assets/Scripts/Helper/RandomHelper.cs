﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Security.Cryptography;

public static class RandomHelper
{
    
    public static Int32 GenerateRandomInt(Int32 min, Int32 max)
    {
        RNGCryptoServiceProvider p = new RNGCryptoServiceProvider();
        byte[] buffer = new byte[32];
        if (min > max)
        {
            throw new ArgumentOutOfRangeException("minValue");
        }
        if (min == max)
        {
            return min;
        }
        Int64 difference = max - min;
        while (true)
        {
            p.GetBytes(buffer);
            UInt32 rand = BitConverter.ToUInt32(buffer, 0);

            Int64 maximum = (1 + (Int64)UInt32.MaxValue);
            Int64 remainder = maximum % difference;
            if(rand < maximum - remainder)
            {
                return(Int32)(min + (rand % difference));
            }
        }
    }
}
