﻿using System.Collections.Generic;
using UnityEngine;
using System.Security.Cryptography;
using System;

public static class ExtensionMethods
{
    //Fisher-Yates shuffle
    public static void PoolShuffle<T>(this IList<T> List)
    {
        RNGCryptoServiceProvider p = new RNGCryptoServiceProvider();
        int n = List.Count;
        while (n > 1)
        {
            byte[] box = new byte[1];
            do
            {
                p.GetBytes(box);
            } while (!(box[0] < n * (byte.MaxValue / n)));

            int k = (box[0] % n);
            n--;
            T value = List[k];
            List[k] = List[n];
            List[n] = value;
        }
    }
}
