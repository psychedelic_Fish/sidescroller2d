﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class MainMenuSceneController : SceneController
{
    [SerializeField] private ScoresContainer scores;
    [SerializeField] private TMPro.TMP_InputField playerName;
    [SerializeField] private GameObject tutePanel;

    private string nameOfPlayer;

    private void Start()
    {
        nameOfPlayer = "Unknown";
    }

    public override void ChangeScene(int i)
    {
        SceneManager.LoadScene(i);
    }
    public void ShowTutePanel()
    {
        tutePanel.SetActive(true);
        playerName.gameObject.SetActive(false);
    }
    public void OnNoPressed()
    {
        scores.CreateCurrentScore(nameOfPlayer, 0);
        ChangeScene(1);
    }

    public void OnYesPressed()
    {
        scores.CreateCurrentScore(nameOfPlayer, 0);
        ChangeScene(4);
    } 
    public void OnLeaderBoardPressed()
    {
        ChangeScene(2);
    }
    public void OnCreditPressed()
    {
        ChangeScene(3);
    }
    public void OnExitPressed()
    {
        Application.Quit();
    }

    public void SetPlayerName()
    {
        nameOfPlayer = playerName.text;
    }
    public void OnInputNameClicked()
    {
        playerName.text = " ";
    }
}
