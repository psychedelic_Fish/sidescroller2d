﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LeaderBoardSceneController : SceneController
{
    public delegate void OnClear();
    public static event OnClear onClear;

    [SerializeField] private GameObject sadPanel;
    [SerializeField] private ScoresContainer scores;

    public override void ChangeScene(int i)
    {
        SceneManager.LoadScene(i);
    }

    public void OnBackClicked()
    {
        ChangeScene(0);
    }

    public void OnClearClicked()
    {
        if(scores.GetCurrentNumberOfScores() == 0)
        {
            return;
        }
        sadPanel.SetActive(true);
    }

    public void OnRealClearClicked()
    {
        scores.ClearScores();
        sadPanel.SetActive(false);
        onClear();
    }

    public void OnCancel()
    {
        sadPanel.SetActive(false);
    }

}
