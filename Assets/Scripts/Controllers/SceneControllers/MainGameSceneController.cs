﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainGameSceneController : SceneController
{
    public override void ChangeScene(int i)
    {
        SceneManager.LoadScene(i);
    }

    public void OnPlayerLose()
    {
        ChangeScene(3);
    }
}
