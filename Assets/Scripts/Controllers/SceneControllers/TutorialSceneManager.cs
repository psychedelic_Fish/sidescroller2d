﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TutorialSceneManager : SceneController
{
    [SerializeField] private ScoresContainer scores;

    public override void ChangeScene(int i)
    {
        SceneManager.LoadScene(i);
    }
    // Start is called before the first frame update
    public void OnNextPressed()
    {
        ChangeScene(1);
    }

    public void BackToMainPressed()
    {
        ChangeScene(0);
    }

}
