﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CreditSceneController : SceneController
{
    public override void ChangeScene(int i)
    {
        SceneManager.LoadScene(i);
    }

    public void MainMenuPressed()
    {
        ChangeScene(0);
    }
    public void LeaderBoardPressed()
    {
        ChangeScene(2);
    }
}
