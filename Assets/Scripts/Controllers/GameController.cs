﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Security.Cryptography;
using System;

[System.Serializable]
public class GameController : MonoBehaviour
{
    public static GameController instance;

    #region Private Variables
    bool playerAlive;
    bool startMovingWorld;
    bool gameOverHit;
    int levelPiecesinScene;
    int collectables;
    string playerName;
    private GameObject lastPlacedLevelPiece;
    #endregion

    #region Properties
    public bool Alive { get => playerAlive; }
    public bool MoveWorld { get => startMovingWorld; }
    public Transform Player { get => player; }
    public float MaxXDistance { get => maxXDistance; }
    public float WorldMoveSpeed { get => worldMoveSpeed; }
    public bool GameOverHit { get => gameOverHit; set => gameOverHit = value; }
    #endregion

    #region SerializeField Private Variables
    [Header("Player transform")]
    [SerializeField] private Transform player;

    [Header("UI Stuff")]
    [SerializeField] private GameObject mainGameUI;
    [SerializeField] private Button readyButton;
    [SerializeField] private Text score;

    [Header("Controllers/Spawners")]
    [SerializeField] private MainGameSceneController sceneController;
    [SerializeField] private LevelSpawner spawner;

    [Header("Score")]
    [SerializeField] private ScoresContainer scoreContainer;

    [Header("World building")]
    [SerializeField] private GameObject tutorialPiece;

    [Tooltip("Minimum number of Level pieces to be in the scene at anyone time")]
    [SerializeField] private int MinNumberOfPiecesInScene;

    [Tooltip("The distance a level piece can go behind the player on the X axis before it is returned")]
    [SerializeField] private float maxXDistance;

    [Tooltip("How fast the world will move towards the player")]
    [SerializeField] private float worldMoveSpeed;
    #endregion


    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);//Make sure to destroy any secondary singletons that spawn(shouldn't happen but you never know)
        }
        instance = this;
    }

    private void Start()
    {
        //Set Alive to true and Moving to false as we dont want the world to start moving straight away
        playerAlive = true;
        startMovingWorld = false;
        playerName = "Unknown"; //Here in case the player doesnt enter a name, anything is better then blank :)

        //Set up some of the functions that need to be called through events
        GameOverCollider.gameOver += GameOverCalled;
        Move.pieceRemoved += DecrementPiecesInScene;
        Collectable.collect += IncreaseCollectableCount;

        //Make our lastPlacedLevelPiece equal to the first piece spawned in the scene using 
        //the intial piece that is already placed
        lastPlacedLevelPiece = spawner.PlaceIntialSpawn(tutorialPiece);
        levelPiecesinScene = 2; //Pieces in the scene at this point is equal to 2
        
    }

    //Called whenever a level piece is returned to the object pool
    void DecrementPiecesInScene()
    {
        levelPiecesinScene--;
    }

    //This function is called when the ready button is pressed
    public void StartGame()
    {
        startMovingWorld = true; //Set the this bool to true so as to start the world moving
        readyButton.gameObject.SetActive(false); //Hide the button away so that it is not in the way
    }

    //This function will be called when the player either hits an obstacle or falls through the map
    void GameOverCalled()
    {
        //These 2 variables when set to false will stop the world from moving
        startMovingWorld = false; 
        playerAlive = false;

        //passing the players score through to the score tracker
        scoreContainer.SetCurrentScore(collectables);
        scoreContainer.AddScoreToContainer();
        sceneController.OnPlayerLose(); //Switch the scene to the credits
    }

    //Called whenever a collectable is picked up
    void IncreaseCollectableCount()
    {
        collectables++; //just increases the collectables count by one
    }

    private void Update()
    {
        if(startMovingWorld && playerAlive)
        {
            //Calculate whether or not we need to spawn a new section of the world
            if(levelPiecesinScene < MinNumberOfPiecesInScene)
            {
                int i = RandomHelper.GenerateRandomInt(0, LevelPooler.instance.PoolCount); //Random number between 0 and the size of the object pool
                Debug.Log(i);
                lastPlacedLevelPiece = spawner.SpawnSection(i); //Set the last placed level piece to the piece we are spawning 
                levelPiecesinScene++; //increase our count of level pieces active in the scene
            }

            score.text = "Score: " + collectables.ToString(); //print our current score to the text box
        }
    }

}
