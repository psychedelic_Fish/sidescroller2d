﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayFinalScore : MonoBehaviour
{
    [SerializeField] private ScoresContainer scores;
    [SerializeField] private Text score;
    void Start()
    {
        score.text = "Final Score: " + scores.CurrentTrackedScore.Score.ToString();
    }
}
