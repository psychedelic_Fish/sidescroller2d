﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableHandler : MonoBehaviour
{
    private List<Collectable> collectables = new List<Collectable>();
    
    // Start is called before the first frame update
    void Start()
    {
        foreach(Collectable c in gameObject.GetComponentsInChildren<Collectable>())
        {
            collectables.Add(c);
        }
    }

    private void OnEnable()
    {
        foreach (Collectable c in collectables)
        {
            if (!c.gameObject.activeSelf)
            {
                c.gameObject.SetActive(true);
            }
        }
    }
}
