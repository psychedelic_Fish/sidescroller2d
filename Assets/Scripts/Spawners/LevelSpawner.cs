﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSpawner : MonoBehaviour
{
    /*Reference to the last spawned level piece
     * this is used to spawn the next piece*/
    GameObject lastSpawnedTransform; 

    //Function to spawn a level piece, int i is the index from which to spawn from in the object pool
    public GameObject SpawnSection(int i)
    {
        var temp = LevelPooler.instance.GetObjectFromPool(i); //Grab the object from the pooler
        /*Find the object name "EndPoint" in the last spawned object
         this is used to snap the level sections together*/
        var snapPoint = lastSpawnedTransform.transform.Find("EndPoint");
        //Snap the sections together
        temp.transform.position = snapPoint.position;
        //Set the newly snapped object to active
        temp.gameObject.SetActive(true);
        //Set the last spawned gameobject to the newly snapped one
        lastSpawnedTransform = temp;
        return temp; //return the newly placed object
    }

    /*Function for placing the intial spawn
     *takes a GameObject as a parameter, this will be the
     * tutorial section of the level*/
    public GameObject PlaceIntialSpawn(GameObject t)
    {
        var snapPoint = t.transform.Find("EndPoint"); //Find the "EndPoint" used to snap
        var temp = LevelPooler.instance.GetObjectFromPool(0);//Grab and object from the pooler 
        
        
        temp.transform.position = snapPoint.position; //Snap the object
        temp.SetActive(true); //Set it to active

        lastSpawnedTransform = temp; //Set the lastSpawned object to the newly placed one
        return temp; //return the newly placed object
    }
}
