﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Security.Cryptography;

public class LevelPooler : MonoBehaviour
{
    public static LevelPooler instance;
    private int poolCount;
    public int PoolCount { get => poolCount; }

    [Tooltip("Drag and drop the GameObjects you want to pool, only needs to be one of each")]
    public List<GameObject> ObjectsTopool = new List<GameObject>(); 
    List<GameObject> pool = new List<GameObject>();
    // Start is called before the first frame update
    void Awake()
    {
        if(instance != null)
        {
            Destroy(gameObject);//Make sure to destroy any secondary instances of the singleton(unlikely)
        }
        instance = this;
        //Go through each object that the user wants to pool
        foreach (GameObject t in ObjectsTopool)
        {
            int rint = Random.Range(1, 10);//Pick a number between 1 and 10
            for (int i = 0; i < rint; i++)
            {
                //Spawn that random number of that particular level piece somewhere out in space
                var temp = Instantiate(t, new Vector2(100, 100), Quaternion.identity);
                temp.SetActive(false); //Make sure they are inactive 
                pool.Add(temp); //Add them to the pool
            }
        }
        poolCount = pool.Count; //Grab a reference to how many pieces are in the pool as we don't want to make the pool public 
                                //but may need that number elsewhere
        pool.PoolShuffle();
    }

    //Function to get an object from the pool
    //Takes an integer as a parameter
    //This integer is used to grab the object from the pool at that index
    public GameObject GetObjectFromPool(int i)
    {
        if(i > pool.Count)
        {
            return null; //if our number is greater then the number of items in the pool return out(just a safe guard)
        }
        else
        {
            if (pool[i].activeSelf) //If the object we want is already active in the scene
            {
                for(int y = 0; y < pool.Count; y++)
                {
                    if (!pool[y].activeSelf)
                    {
                        return pool[y]; //find the next object that isnt active and return it 
                    }
                }
                return null; //otherwise return null (again just a safegaurd)
            }
            else
            {
                pool[i].SetActive(true);//Other wise we just return our picked object and set it to active
                return pool[i];
            }
        }
    }

    //Called if we want to return an object to the pool
    //Takes a GameObject as a parameter
    public void ReturnObjectToPool(GameObject t)
    {
        t.SetActive(false);//If we return the passed in object back to the pool it needs to be inactive
    }

}
