﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ScoreContainer", menuName = "Score Stuff")]
[System.Serializable]
public class ScoresContainer : ScriptableObject
{
    private List<ScoreData> scores = new List<ScoreData>(); //List of stored score data
    private ScoreData currentTrackedScore; //current players score data
    public ScoreData CurrentTrackedScore { get => currentTrackedScore;} //Property to obtain the current score data

    //Struct that contains the necessary information about scores
    [System.Serializable]
    public struct ScoreData : IComparable<ScoreData>
    {
        public string playerName;
        public int Score;

        public int CompareTo(ScoreData other)
        {
            return other.Score.CompareTo(Score);
        }
    }

    //Function to add a score to the scores list
    public void AddScoreToContainer()
    {
        scores.Add(currentTrackedScore);//Always adds the currentTrackedScore
        scores.Sort();//Sort the list by score 
    }
    public void CreateCurrentScore(string n, int s)
    {
        currentTrackedScore = new ScoreData { playerName = n, Score = s };//need to create the currenttracked score
    }
    public void ClearScores()
    {
        scores.Clear();//Clears the scorelist
    }
    //Returns the scoreData at the passed in index 
    //Here so the list can be private
    public ScoreData GetScoreData(int i)
    {
        return scores[i];
    }

    public void SetCurrentScore(int c)
    {
        currentTrackedScore.Score = c;
    }
    public int GetCurrentNumberOfScores()
    {
        return scores.Count;
    }
}
