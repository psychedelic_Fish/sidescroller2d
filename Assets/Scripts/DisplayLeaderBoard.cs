﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayLeaderBoard : MonoBehaviour
{
    

    [SerializeField] private ScoresContainer scores;
    // Start is called before the first frame update
    void Start()
    {
        for(int i = 0; i < scores.GetCurrentNumberOfScores(); i++)
        {
            GameObject newTextObject = new GameObject("Text");
            newTextObject.transform.SetParent(transform);

            Text mytext = newTextObject.AddComponent<Text>();
            Font ArialFont = (Font)Resources.GetBuiltinResource(typeof(Font), "Arial.ttf");
            mytext.font = ArialFont;
            mytext.rectTransform.localScale = new Vector3(1, 1, 1);
            mytext.rectTransform.sizeDelta = new Vector2(1055, 100);
            mytext.resizeTextForBestFit = true;
            mytext.resizeTextMaxSize = 100;
            mytext.text = scores.GetScoreData(i).playerName.ToString() + " " + "Score: " + scores.GetScoreData(i).Score.ToString();
        }
        LeaderBoardSceneController.onClear += ClearLeaderBoard;
        
    }

    void ClearLeaderBoard()
    {
        foreach(Transform child in transform)
        {
            Destroy(child.gameObject);
        }
    }
}
