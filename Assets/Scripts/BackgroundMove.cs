﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundMove : MonoBehaviour
{
    Vector3 offset;
    [SerializeField] private Transform mainCamera;

    private void Start()
    {
        offset = transform.position - mainCamera.position;
    }
    // Update is called once per frame
    void LateUpdate()
    {
        transform.position = mainCamera.position + offset;
    }
}
